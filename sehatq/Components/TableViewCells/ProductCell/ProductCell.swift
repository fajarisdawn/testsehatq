//
//  ProductCell.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {

    @IBOutlet weak var contentDescLabel: UILabel!
    @IBOutlet weak var contentPriceLabel: UILabel!
    @IBOutlet weak var contentTitleLabel: UILabel!
    @IBOutlet weak var contentImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(_ product: Product) {
        contentDescLabel.text = product.desc
        contentPriceLabel.text = product.price
        contentTitleLabel.text = product.title.capitalized
        contentImageView.kf.setImage(with: URL(string: product.imageUrl))
    }
}

extension ProductCell: ReusableCell {}
