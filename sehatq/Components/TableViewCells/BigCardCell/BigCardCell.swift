//
//  BigCardCell.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit
import Kingfisher

class BigCardCell: UITableViewCell {

    @IBOutlet weak var likeButton: LikeButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        likeButton.addTarget(self, action: #selector(likeButtonPressed(_:)), for: .touchUpInside)
    }
    
    func configureCell(_ product: Product) {
        containerView.setCornerRadius(8.0)
        self.productImageView.kf.setImage(with: URL(string: product.imageUrl))
        self.titleLabel.text = product.title.capitalized
        self.descLabel.text = product.desc
    }
}


//  MARK: - Set Action
extension BigCardCell {
    @objc @IBAction func likeButtonPressed(_ sender: UIButton) {
        likeButton.setLike(!likeButton.status)
    }
}

extension BigCardCell: ReusableCell {}
