//
//  LikeButton.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

class LikeButton: UIButton {
    var status: Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = .clear
        setLike(false)
    }
    
    func setLike(_ status: Bool) {
        if status {
            self.setImage( .imageType(.like) , for: .normal)
        } else {
            self.setImage( .imageType(.unLike) , for: .normal)
        }
        self.status = status
    }
    
    func setEnabled(_ status: Bool) {
        self.isUserInteractionEnabled = status
    }
}
