//
//  CategoryCell.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ category: Category) {
        iconImageView.kf.setImage(with: URL(string: category.imageUrl))
        titleLabel.text = category.name.capitalized
    }

}

extension CategoryCell: ReusableCell {}
