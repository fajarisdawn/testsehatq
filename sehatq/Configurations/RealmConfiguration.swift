//
//  RealmConfiguration.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 16/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit
import RealmSwift

struct RealmConfiguration {
    static func config() {
        guard let buildVersion = Bundle.main.infoDictionary! ["CFBundleVersion"] as? String else { return }
        let config = Realm.Configuration(
            schemaVersion: UInt64.init(CGFloat(Int(buildVersion) ?? 0)),
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 1 {}
        })
        Realm.Configuration.defaultConfiguration = config
    }
}
