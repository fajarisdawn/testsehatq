//
//  APINetwork.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 16/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation
import Moya

enum APINetwork {
    case getData
}

extension APINetwork: TargetType {
    
    var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8) ?? Data()
    }
    
    //  MARK: - Set Network Base Url
    public var baseURL: URL {
        return URL(string: NetworkAttribute.baseURL)!
    }
    
    public var path: String {
        switch self {
        case .getData:
            return "home"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .getData:
            return .get
        }
    }
    
    public var task: Task {
        return .requestPlain
    }
    
    public var parameters: [String:Any] {
        return [String:Any]()
    }
    
    public var urlParameters: [String:Any] {
        return [String:Any]()
    }
    
    public var headers: [String: String]? {
        return [String:String]()
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
    
    //  MARK: - Set Network Parameter Encoding
    var parameterEncoding: Moya.ParameterEncoding {
        return JSONEncoding.default
    }
}

