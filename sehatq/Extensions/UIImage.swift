//
//  UIImage.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

enum ImageType {
    case like, unLike
}

extension UIImage {
    class func imageType(_ type: ImageType) -> UIImage {
        var image = UIImage()
        switch type {
        case .like:
            image = UIImage(named: ImageAttribute.iconLike)!
        case .unLike:
            image = UIImage(named: ImageAttribute.iconUnlike)!
        }
        return image
    }
}
