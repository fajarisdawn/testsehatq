//
//  TabBarItem.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit


extension UITabBarItem {
    class func customTabBarItem(title: String, image: UIImage, selectedImage: UIImage, tag: Int) -> UITabBarItem {
        let tabItem = UITabBarItem.init(title: title, image: image, selectedImage: selectedImage)
        tabItem.tag = tag
        return tabItem
    }
}
