//
//  UIView.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

extension UIView {
    func setCornerRadius(_ radius: CGFloat) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
    }
}
