//
//  BaseTableViewController.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

protocol BaseTableViewControllerInteractor {
    func selectedRow(_ product: Product)
}

class BaseTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var interactor: BaseTableViewControllerInteractor?
    
    var viewModel: BaseTableViewModel! {
        didSet {
            tableView.reloadData()
        }
    }
    
    init(_ viewModel: BaseTableViewModel) {
        super.init(nibName: "BaseTableViewController", bundle: nil)
        self.viewModel = viewModel
        self.viewModel.interactor = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ProductCell.self)
        tableView.removeEmptyCell = true
    }
}

extension BaseTableViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tempProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configureCell(viewModel.tempProducts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: false)
        DispatchQueue.main.async {
            self.interactor?.selectedRow(self.viewModel.tempProducts[indexPath.row])
        }
    }
}


extension BaseTableViewController: BaseTableViewInteractor {
    func reloadData() {
        tableView.reloadData()
    }
}
