//
//  ProductViewModel.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit
import ObjectMapper

enum BaseTableViewType {
    case product
}

protocol BaseTableViewInput {
    var contents: [Any] { get set }
}

protocol BaseTableViewInteractor {
    func reloadData()
}

final class BaseTableViewModel: BaseTableViewInput {
    var contents: [Any]
    var products: [Product]
    var tempProducts: [Product]
    var interactor: BaseTableViewInteractor?
    
    init(_ contents: [Any], type: BaseTableViewType) {
        self.contents = contents
        switch type {
        case .product:
            let products = contents as! [Product]
            self.products = products
            self.tempProducts = products
        }
    }
    
    func searchContent(_ value: String) {
        if !value.isEmpty {
            tempProducts = [Product]()
            products.forEach { (product) in
                if product.title.contains(value) {
                    tempProducts.append(product)
                }
            }
        } else {
            tempProducts = products
        }
        interactor?.reloadData()
    }
    
}
