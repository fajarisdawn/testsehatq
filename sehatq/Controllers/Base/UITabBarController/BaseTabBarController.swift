//
//  BaseTabBarController.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {
    
    var searchController : UISearchController!
    var products = [Product]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setControllers()
    }
}

extension BaseTabBarController {
    
    private func setSearchBar(_ contents: [Any]) {
        let tableViewModel = BaseTableViewModel.init(contents, type: .product)
        let baseSearchController = SearchViewController(tableViewModel)
        baseSearchController.interactor = self
        searchController = UISearchController(searchResultsController: baseSearchController)
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.animationController(forPresented: baseSearchController, presenting: baseSearchController, source: self)
        self.navigationItem.titleView = searchController.searchBar
        self.definesPresentationContext = true
    }
    
    private func setControllers() {
        let homeViewController = HomeViewController()
        homeViewController.interactor = self
        
        homeViewController.tabBarItem = .customTabBarItem(title: "Home", image: UIImage(), selectedImage: UIImage(), tag: 0)
        let newsViewController = NewsViewController()
        newsViewController.tabBarItem = .customTabBarItem(title: "News", image: UIImage(), selectedImage: UIImage(), tag: 1)
        
        let tableViewModel = BaseTableViewModel.init(Product.all(), type: .product)
        let historyViewController = HistoryViewController.init(tableViewModel)
        historyViewController.tabBarItem = .customTabBarItem(title: "History", image: UIImage(), selectedImage: UIImage(), tag: 2)
        
        let settingsViewController = SettingsViewController()
        settingsViewController.tabBarItem = .customTabBarItem(title: "Settings", image: UIImage(), selectedImage: UIImage(), tag: 3)
        tabBar.backgroundColor = .white
        viewControllers = [homeViewController, newsViewController, historyViewController, settingsViewController]
    }
}

extension BaseTabBarController: UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {}
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        let viewModel = BaseTableViewModel(products, type: .product)
//        redirectTo(BaseTableViewController(viewModel))
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let controller = searchController.searchResultsController as? SearchViewController else { return }
        controller.viewModel.searchContent(searchText)
    }
}

extension BaseTabBarController: HomeControllerInteractor {
    func getContents(_ contents: [Any]) {
        setSearchBar(contents)
        products = contents as! [Product]
    }
}

extension BaseTabBarController: BaseTableViewControllerInteractor {
    func selectedRow(_ product: Product) {
        let viewModel = ShowProductViewModel(product)
        redirectTo(ShowProductController(viewModel))
    }
}
