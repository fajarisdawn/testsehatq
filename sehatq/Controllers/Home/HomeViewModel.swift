//
//  DashboardViewModel.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 16/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

protocol HomeInput {
    var categories: [Category] { get set }
    var promos: [Product] { get set }
    var networkService: GeneralNetworkService { get set }
}

protocol HomeViewModelInteractor {
    func successLoadData()
    func failedLoadData(error: Error)
}

final class HomeViewModel: HomeInput {
    var categories: [Category]
    var promos: [Product]
    var networkService: GeneralNetworkService
    var interactor: HomeViewModelInteractor?
    
    
    init() {
        self.networkService = GeneralNetworkService()
        self.categories = [Category]()
        self.promos = [Product]()
        self.networkService.interactor = self
        networkService.request(.getData)
    }
    
    func inquireData() {
        
    }
}

//  MARK: - General Network Service Interactor
extension HomeViewModel: GeneralNetworkServiceInteractor {
    func success(_ object: NetworkData, type: APINetwork) {
        guard let data = object.data else { return }
        self.categories = data.category ?? [Category]()
        self.promos = data.promos ?? [Product]()
        interactor?.successLoadData()
    }
    
    func failed(_ error: Error, type: APINetwork) {
        interactor?.failedLoadData(error: error)
    }
}
