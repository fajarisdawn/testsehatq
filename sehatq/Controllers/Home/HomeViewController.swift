//
//  DashboardViewController.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 16/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

protocol HomeControllerInteractor {
    func getContents(_ contents: [Any])
}

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let viewModel = HomeViewModel()
    var interactor: HomeControllerInteractor?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.interactor = self
        setComponents()
    }
}

//  MARK: - Set Components
extension HomeViewController {
    private func setComponents() {
        //  Configure CollectionView
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CategoryCell.self)
        collectionView.setCornerRadius(10.0)
        
        //  Configure TableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(BigCardCell.self)
        tableView.removeEmptyCell = true
        tableView.separatorStyle = .none
    }
}

//  MARK: - CollectionView Delegate & Data Source
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CategoryCell = collectionView.dequeueReusableCell(indexPath: indexPath)
        cell.configureCell(viewModel.categories[indexPath.row])
        return cell
    }
    
    //  Flow Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.size.width / 4) * 0.8
        let height = view.frame.size.width / 4
        return CGSize(width: width, height: height)
    }
}

//  MARK: - UITableView Delegate & Data Source
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.promos.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BigCardCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configureCell(viewModel.promos[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = ShowProductViewModel(self.viewModel.promos[indexPath.row])
        redirectTo(ShowProductController(viewModel))
    }
}



extension HomeViewController: HomeViewModelInteractor {
    func successLoadData() {
        collectionView.reloadData()
        tableView.reloadData()
        interactor?.getContents(viewModel.promos)
    }
    
    func failedLoadData(error: Error) {
        
    }
}
