//
//  HistoryViewController.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

class HistoryViewController: BaseTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "History"
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = ShowProductViewModel(self.viewModel.products[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: false)
        redirectTo(ShowProductController(viewModel))
    }
}
