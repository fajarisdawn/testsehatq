//
//  ShowProductViewModel.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

protocol ShowProductInput {
    var product: Product { get set }
}

class ShowProductViewModel: ShowProductInput {
    var product: Product
    
    init(_ product: Product) {
        self.product = product
    }
}
