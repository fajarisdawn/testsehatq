//
//  ShowProductController.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

class ShowProductController: UIViewController {
    
    @IBOutlet weak var contentViewVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var lovedCount: UILabel!
    @IBOutlet weak var buyButton: RoundedButton!
    @IBOutlet weak var likeButton: LikeButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var viewModel: ShowProductViewModel!
    
    init(_ viewModel: ShowProductViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "ShowProductController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setComponents()
    }
}

//  MARK: - Set Components
extension ShowProductController {
    private func setComponents() {
        navigationItem.title = viewModel.product.title
        descLabel.text = viewModel.product.desc
        titleLabel.text = viewModel.product.title.capitalized
        priceLabel.text = viewModel.product.price
        lovedCount.text = String(describing: viewModel.product.loved)
        imageView.kf.setImage(with: URL.init(string: viewModel.product.imageUrl))
        contentViewVerticalConstraint.constant = descLabel.bounds.size.height + 20
        likeButton.setEnabled(false)
        likeButton.setLike(true)
        buyButton.addTarget(self, action: #selector(buyButtonPressed(_:)), for: .touchUpInside)
    }
    
    @objc @IBAction func buyButtonPressed(_ sender: UIButton) {
        Product.create(object: viewModel.product)
        navigationController?.popViewController(animated: true)
    }
}
