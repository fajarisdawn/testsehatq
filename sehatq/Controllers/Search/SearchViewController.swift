//
//  SearchViewController.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 18/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

class SearchViewController: BaseTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.frame.origin.y = 100
    }
}
