//
//  NetworkAttributes.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 16/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

struct NetworkAttribute {
    static let baseURL = "https://private-4639ce-ecommerce56.apiary-mock.com/"
}
