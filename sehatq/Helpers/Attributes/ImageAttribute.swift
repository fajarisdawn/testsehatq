//
//  ImageAttribute.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 17/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

struct ImageAttribute {
    static var iconLike = "icon_like"
    static var iconUnlike = "icon_unlike"
}
