//
//  ErrorAttribute.swift
//  sehatq
//
//  Created by Fajar Adiwa Sentosa on 16/07/19.
//  Copyright © 2019 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

struct ErrorAttribute {
    static let serverUnderMaintenance = "Server under maintenace"
}
